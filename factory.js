/*
 * Factory Design Pattern
 * 1. Basic Factory
 */
function VehicleFactory () {}

VehicleFactory.prototype.vehicleClass = Car

VehicleFactory.prototype-getVehicle = function (options) {
	return new this.vehicleClass(options)
}

var carFactory = new VehicleFactory()
var car = carFactory.getVehicle({
	color : 'yellow',
	turbo : true
})

/*
 * 2. Modify a VehicleFactory instance
 */
carFactory.vehicleClass = Truck

var mover = carFactory.getVehicle({
	enclosedCargo : true,
	length : 26
})

function TruckFactory {}
TruckFactory.prototype = new VehicleFactory()
TruckFactory.prototype.vehicleClass = Truck

var truckFactory = new TruckFactory()
var bigfoot = truckFactory.getVehicle({
	monster : true,
	cylinders : 12
})

/*
 * Abstract Factory Pattern
 */
var AbstractVehicleFactory = (function () {
	var types = {}
	
	return {
		getVehicle : function (type, customizations) {
			var Vehicle = types[type]
			
			return (Vehicle) ? return new Vehicle(customizations) : null
		},
		registerVehicle : function (type, Vehicle) {
			var proto = Vehicle.prototype
			
			if (proto.drive && proto.breakDown) {
				types[type] = Vehicle
			}
			
			return AbstractVehicleFactory
		}
	}
})()

/*
 * Usage
 */
AbstractVehicleFactory.registerVehicle('car', Car)
AbstractVehicleFactory.registerVehicle('truck', Truck)

var car = AbstractVehicleFactory.getVehicle('car', {
	color : 'yellow',
	turb: true
})

var truck = AbstractVehicleFactory.getVehicle('truck', {
	monster : true,
	cylinders : 12
})
