/*
 * How-to create an Object
 * 1. Literal
 * 2. Using Object.create()
 * 3. Using new Object()
 */
var literal_object = {}
var create_object = Object.create(null)
var new_object = new Object()

/*
 * Wrong!
 */
var wrong_object = Object()

/*
 * ECMAScript 3 compatible
 * 1. Dot Syntax
 * 2. Square bracket syntax
 */
new_object.someKey = 'Hello world'
var key = new_object.someKey

new_object['someKey'] = 'Hello world'
var key = new_object['someKey']

/*
 * ECMAScript 5
 * 3.a Object.defineProperty
 * 3.b Short-hand
 * 4. Object.defineProperties()
 */
Object.defineProperty(new_object, 'someKey', {
	value : 'For more control fo the property\'s behavior',
	writable : true,
	enumarable : true,
	configurable : true
})

// Short-hand
function defineProp (obj, key, value) {
	config.value = value
	Object.defineProperty(obj, key, config)
}

var man = Object.create(null)
defineProp(man, 'car', 'Delorean')
defineProp(man, 'dob', '1981')
defineProp(man, 'beard', false)

// Object.defineProperties()
Object.defineProperties(new_object, {
	'someKey' : {
		value : 'Hello world',
		writable : true
	},
	'anotherKey' : {
		value : 'Foo Bar',
		writable : false
	}
})

var driver = Object.create(null)
defineProp(driver, 'topSpeed', '110mph')
driver.topSpeed
