var pubsub = {}

var PubSub = (function (q) {
	var topics = {}
	var sub_uid = -1
	
	q.publish = function (topic, args) {
		if (!topics[topic]) {
			return false
		}
		
		var subscribers = topics[topic]
		var len = subscribers ? subscribers.length : 0
		
		while (len--) {
			subscribers[len].func(topic, args)
		}
		
		return this
	}
	
	q.subscribe = function (topic, func) {
		if (!topics[topic]) {
			topics[topic] = []
		}
		
		var token = (++sub_uid).toString()
		
		topics[topic].push({
			token : token,
			func : func
		})
	}
	
	q.unsubscribe = function (token) {
		for (var m in topics) {
			if (topics[m]) {
				for (var i = 0, i = topics[m].length; i < j; i++) {
					if (topics[m][i].token == token) {
						topics[m].splice(i, 1)
						return token
					}
				}
			}
		}
		
		return this
	}
})(pubsub)

/*
 * Testing pubsub
 */
var test_handler = function (topic, data) {
	console.log(topics + ': ' + data)
}

var test_subscription = pubsub.subscribe('example1', test_handler)

pubsub.publish('example1', 'foo')
pubsub.publish('example1', ['foo','bar','baz'])
pubsub.publish('example1', [
	{ 'color' : 'blue'},
	{ 'text' : 'foo bar baz'}
])

pubsub.unsubscribe('example1', 'hello (this will fail)')

/*
 + Testing pubsub - grid
 */
var grid = {
	refresh_data : function () {
		console.log('retrieve all data')
		console.log('update grid component')
	},
	update_counter : function () {
		console.log('data last updated: ' + get_current_time())
	}
}

var grid_update = function (topics, ata) {
	grid.refresh_data()
	grid.update_counter()
}

var data_subscription = PubSub.subscribe('data_updated', grid_update)
PubSub.publish('data_updated', 'new stock available')
PubSub.publish('data_updated', 'new stock available')

function get_current_time () {
	var date = new Date()
	var m = date.getMonth() + 1
	var d = date.getDate()
	var y = date.getFullYear()
	var t = date.toLocaleTimeString().toLowerCase()
	
	return (m + '/' + d + '/' + y + ' ' + t)
}

/*
 * Testing pubsub - Grid improved
 */
var grid = {
	add_entry : function (data) {
		if (data) {
			var msg = 'Entry: ' + data.title +
								' Changed at / %' + data.changenet +
								'/' + data.percentage + ' % added'
								
			console.log(msg)
		}
	},
	
	update_counter : function (timestamp) {
		console.log('grid last updated at: ' + timestamp)
	}
}

var grid_update = function (topics, data) {
	grid.add_entry(data)
	grid.update_counter(data.timestamp)
}

var grid_subscription = PubSub.subscribe('data_updated', grid_update)

PubSub.publish('data_updated', {
	title : 'Microsoft Shares',
	changenet : 4,
	percentage : 33,
	timestamp : '17:34:12'
})

PubSub.publish('data_updated', {
	title : 'Dell Shares',
	changenet : 10,
	percentage : 20,
	timestamp : '17:35:16'
})
