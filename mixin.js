var Car = function (settings) {
	this.model = settings.model || '[model]: undefined'
	this.color = settings.color || '[color]: undefined'
}

var Mixin = function () {}

Mixin.prototype = {
	driveForward : function () {
		console.log('Drive forward')
	},
	driveBackward : function () {
		console.log('Drive backward')
	}
}

function augment (receivingClass, givingClass) {
	if (arguments[2]) {
		for (var i = 2, len = arguments.length; i < len; i++) {
			receivingClass.prototype[arguments[i]] = givingClass.prototype[arguments[i]]
		}
	} else {
		for (var methodName in givingClass.prototype) {
			if (!receivingClass.prototype[methodName]) {
				receivingClass.prototype[methodName] = givingClass.prototype[methodName]
			}
		}
	}
}

augment(Car, Mixin, 'driveForward', 'driveBackward')

var vehicle = new Car({
	model : 'Ford Escort',
	color : 'blue'
})

vehicle.driveForward()
vehicle.driveBackward()
