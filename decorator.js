/*
 * Decorator Design Pattern
 * 1. Basic Decoration
 */
function vehicle (vehicleType) {
	this.vehicleType = vehicleType
	this.model = '[default]'
	this.license = '0000-00'
}

var testInstance = new vehicle('car')
var truck = new vehicle('truck')

truck.setModel = function (modelName) {
	this.model = modelName
}

truck.setColor = function (color) {
	this.color = color
}

truck.setModel('CAT')
truck.setColor('blue')

/*
 * 2. Simply decorator objects with multiple decorators
 */
function MacBook () {
	this.cost = function () {
		return 997
	}
	
	this.screenSize = function {
		return 13.3
	}
}

/* Decorator one */
function Memory (macbook) {
	var v = macbook.cost()
	
	macbook.cost = function () {
		return v + 75
	}
}

/* Decorator 2 */
function Engraving (macbook) {
	var v = macbook.cost()
	
	macbook.cost = function () {
		return v + 200
	}
}

/* Decorator 3 */
function Insurance (macbook) {
	var v = macbook.cost()
	
	macbook.cost = function () {
		return v + 250
	}
}

var mb = new MacBook()

Memory(mb)
Engraving(mb)
Insurance(mb)

console.log(mb.cost())
