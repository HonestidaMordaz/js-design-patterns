/*
 + Singleton pattern
 * 1. Literal singleton
 */
var singleton = {
	prop : 'something',
	method : function () {
		console.log('foo bar')
	}
}

/*
 + 2. Singleton + module-based pattern
 */
var singleton = function () {
	var private_variable = 'really private'
	
	function show_private () {
		console.log(private_variable)
	}
	
	return {
		public_method : function () {
			show_private()
		},
		public_var : 'very public'
	}
}

var single = singleton()
single.public_method()
console.log(single.public_var)

/*
 * 3. Singleton return instance
 */
var Singleton = (function () {
	var instance
	
	function init () {
		return {
			public_method : function () {
				console.log('foo')
			},
			public_property : 'bar'
		}
	}
	
	return {
		get_instance : function () {
			if (!instance) {
				instance = init()
			}
			
			return instance
		}
	}
})()

Singleton.get_instance().public_method()

/*
 * 4. Singleton + return instance + config
 */
var Singleton = (function () {
	function Singleton (options) {
		options = options || {}
		this.name = 'Singleton'
		this.point_x = args.point_x || 6
		this.point_y = args.point_y || 10
	}
	
	var instance	
	var _static = {
		name : 'Singleton',
		get_instance : function (options) {
			if (!instance) {
				instance = new Singleton(options)
			}
			
			return instance
		}
	}
	
	return _static
})()

var single = Singleton.get_instance({
	point_x : 5
})

console.log(single.point_x)
