/*
 * Revealing Module Pattern
 * 1. Module self-contained
 */
var my_module = (function () {
	var name = 'John Doe'
	var age = 40
	
	function update_person () {
		name = 'John Doe Updated'
	}
	
	function set_person () {
		name = 'John Doe Set'
	}
	
	function get_person () {
		return name
	}
	
	return {
		set : set_person,
		get : get_person
	}
})()

my_module.get()
