/*
 * Async Module Definition
 * 1. Example
 */
define('myModule', ['foo', 'bar'],  function (foo, bar) {
	var myModule = {
		doStuff : function () {
			console.log('foo')
		}
	}
	
	return myModule
})

// Alternative version
define('myModule', ['math', 'graph'], function (math, graph) {
	return {
		plot : function (x, y) {
			return graph.drawPie(math.randomGrid(x, y))
		}
	}
})

/*
 * Require the module
 */
require(['foo', 'bar'], function (foo, bar) {
	foo.doSomething()
})

define(function (require) {
	var isReady = false
	var foobar
	
	require(['foo', 'bar'], function (foo, bar) {
		isReady = true
		foobar = foo() + bar()
	})
	
	return {
		isReady : isReady,
		foobar : foobar
	}
})
