/*
 * Command Design Pattern
 */
(function () {
	var car_manager = {
		request_info : function (model, id) {
			return '[model]:' + model + '; [id]:' + id
		},
		buy_vehicle : function (model, id) {
			return '[id]:' + id + '; [model]:' + model
		},
		arrange_viewing : function (model, id) {
			return '[model]:' + model + '; [id]:' + id
		}
	}
	
	car_manager.execute = function (name) {
		return car_manager[name] && car_manager[name].apply(car_manager, [].slice.call(arguments, 1))
	}
	
	car_manager.execute('buy_vehicle', 'Ford Escort', '434343')
	car_manager.execute('arrange_viewing', 'Ferrari', '434344')
	car_manager.execute('request_info', 'Ford Mondeo', '434345')
})
