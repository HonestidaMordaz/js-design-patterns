/*
 * Prototype pattern
 * 1. Object.create() prototype
 */
var some_car = {
	drive : function () {},
	name : 'Mazda 3'
}

var another_car = Object.create(some_car)
console.log(another_car.name)

// Another example
var vehicle = {
	get_model : function () {
		console.log('[model]: ' + this.model)
	}
}

var car = Object.create(vehicle, {
	'id' : {
		value : MY_GLOBAL.next_id(),
		enumerable : true
	},
	'model' : {
		value : 'Ford',
		enumerable : true
	}
})

/*
 * 2. Manual prototype
 */
var vehicle_prototype = {
	init : function (car_model) {
		this.model = car_model
	},
	get_model : function () {
		console.log('[model]: ' + this.model)
	}
}

function vehicle (model) {
	function F () {}
	F.prototype = vehicle_prototype
	
	var f = new F()
	f.init(model)
	
	return f
}

var car = vehicle('Ford Escort')
car.get_model()

// Alternative version
var beget = (function () {
	function F() {}
	
	return function (proto) {
		F.prototype = proto
		return new F()
	}
})()
