/*
 * Module desing pattern
 * 1. Object literal
 */
var object_literal = {
	variable_key : variable_key,
	function_key : function () {
		// TODO..
	}
}

/*
 * 2. Object literal as conceptual module
 */
var my_module = {
	my_property : 'foo',
	my_config : {
		use_caching : true,
		language : 'en'
	},
	my_method : function () {
		console.log('foo_bar')
	},
	other_method : function () {
		console.log('Caching is: ' + (this.my_config.use_caching) ? 'enabled' : 'disabled')
	},
	some_method : function (new_config) {
		if (typeof new_config == 'object') {
			this.my_config = new_config
			console.log(this.my_config.language)
		}
	}
}

/*
 * 3. Module self-contained
 */
var test_module = (function () {
	var counter = 0
	
	return {
		increment_counter : function () {
			return counter++
		},
		reset_counter : function () {
			console.log(counter)
			counter = 0
		}
	}
})()

test_module.increment_counter()
test_module.reset_counter()

/*
 * 4. Namespacing
 */
var my_namespace = (function (){
	var my_private_var = 0
	
	var my_private_method = function (some_text) {
		console.log(some_text)
	}
	
	return {
		my_public_var : 'foo',
		my_public_function : function (bar) {
			my_private_var++
			my_private_method(bar)
		}
	}
})()

/*
 * Basket Module
 */
var basket_module = (function () {
	var basket = []
	
	function do_something () {
		// TODO...
	}
	
	function do_something_else () {
		// TODO...
	}
	
	return {
		add_item : function (values) {
			basket.push(values)
		},
		get_item_count : function () {
			return basket.length
		},
		do_something : do_something(),
		get_total : function () {
			var q = this.get_item_count()
			var p = 0
			
			while (q--) {
				p += basket[q].price
			}
			
			return p
		}
	}
})()

basket_module.add_item({
	item : 'bread',
	price : 0.5
})

basket_module.add_item({
	item : 'butter',
	price : 0.3
})

/*
 * Template module
 */
var some_module = (function () {
	var private_var = 5
	
	var private_method = function () {
		return 'Private test'
	}
	
	return {
		public_var : 10,
		public_method : function () {
			return 'foo bar baz'
		},
		get_data : function () {
			return private_method() + this.public_method() + private_var
		}
	}
})()

some_module.get_data()
