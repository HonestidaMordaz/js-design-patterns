var Person = function (firstName, lastName) {
	this.firstName = firstName
	this.lastName = lastName
	this.gender = 'male'
}

var SuperHero = function (firstName, lastName, powers) {
	Person.call(this, firstName, lastName)
	this.powers = powers
}

SuperHero.prototype = Object.create(Person.prototype)

var superman = new SuperHero('Clark', 'Kent', ['flight', 'heat-vision'])
